/**
 * Created with IntelliJ IDEA.
 * User: Gleb
 * Date: 19.04.12
 * Time: 21:37
 * To change this template use File | Settings | File Templates.
 */
package {
import com.junkbyte.console.Cc;

import event.RemoteEvent;

import flash.events.EventDispatcher;
import flash.external.ExternalInterface;
import flash.net.URLVariables;

import net.PostConnector;

public class HostService extends EventDispatcher{
    private var urlVars:URLVariables;
    public var service:PostConnector;
    public static const url:String = "http://80.82.81.238:8181/api/g/"; //"http://dev.dressformer.ru/";
    public static var webUrl:String;
    public function HostService() {
        service = new PostConnector(url);
        this.addEventListener(RemoteEvent.SEND_BODY_PARAM, onSendBodyGenParam);
        this.addEventListener(RemoteEvent.SOCIAL_START,onSocialStart);
        this.addEventListener(RemoteEvent.CUSTOM_START,onCustomStart);
        //Cc.info("Current run " + ExternalInterface.call('window.location.href.toString'))
        Cc.info("HostService.url: "+ url);
        if (ExternalInterface.available){
            webUrl = ExternalInterface.call('window.location.href.toString');
            Cc.info("Current url: " + webUrl);
        }
    }



    private function addUrlVars(o:Object=null):void{
        urlVars ||= new URLVariables();
        urlVars.app = "site";
        for (var s:String in o){
            urlVars[s] = o[s];
            Cc.info("'"+s+"' it is new URLVariable added: "+urlVars[s]);
        }
    }

    private function firstRequest():void{
        service.call("Cloth.Category.List",{parentId:1});
        service.call("Cloth.Items.List",{categoryId:1, offset:0});
        service.call("Cloth.Items.Request",{itemId:1});

    }
    private function onCustomStart(event:RemoteEvent):void {
        if(webUrl){
            addUrlVars({app:"site"});
            service.call("Auth.Signup.OAuth",{returnTo:webUrl});
        } else {
            addUrlVars({app:"debug"});
            service.call("Auth.Signup.OAuth",{returnTo:"x.хom"});
        }
        firstRequest();
    }
    private function onSocialStart(event:RemoteEvent=null):void {
        addUrlVars({app:"vk",access_token:SocialService.access_token});
        firstRequest();
    }
    private function onSendBodyGenParam(event:RemoteEvent):void {
        service.call("Person.Body.Parameters",event.data);
        Cc.visible = true;
    }

}
}

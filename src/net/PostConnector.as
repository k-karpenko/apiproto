/**
 * Created with IntelliJ IDEA.
 * User: soul
 * Date: 7/17/12
 * Time: 10:22 AM
 * To change this template use File | Settings | File Templates.
 */
package net {
import com.junkbyte.console.Cc;

import flash.events.Event;
import flash.events.EventDispatcher;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;

public class PostConnector extends EventDispatcher {

    private var host_url:String;
    public function PostConnector(host_url:String) {
        this.host_url = host_url;
    }


    public function call(command:String,params:Object,urlVars:URLVariables=null):void{

        var url:String = host_url+command;
        if (urlVars)
            url += "?"+urlVars.toString();

        var urlRequest:URLRequest = new URLRequest(url);
        urlRequest.method = URLRequestMethod.POST;
        urlRequest.requestHeaders.push(new URLRequestHeader('X-Requested-With', 'XMLHttpRequest'));

        urlRequest.data = JSON.stringify(params);

        var urlLoader:URLCustomLoader = new URLCustomLoader();
        urlLoader.commandName = command;
//        urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
        urlLoader.addEventListener(Event.COMPLETE, chunkComplete);
        urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
        urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
        //urlLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, responseStatusHandler);
        urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, statusHandler);
        urlLoader.addEventListener(ProgressEvent.PROGRESS, progressHandler);
        urlLoader.load(urlRequest);

        Cc.infoch("server","SEND: "+command+ ", JSON:"+JSON.stringify(params));
    }

    protected function chunkComplete(event:Event):void
    {

//        trace("complete",event);
//        trace("reply data: ",event.target.data);
        var ul:URLCustomLoader = event.target as URLCustomLoader;

        Cc.infoch("server","REPLY: "+ul.commandName+", DATA:"+event.target.data);
//        var byteArray:ByteArray = new ByteArray();
//        var ho:Hessian2Output= new Hessian2Output(byteArray);
//        var test:ClothCategory = new ClothCategory();
//        test.name ="yeaahh";
//        test.uniquenessRequired = true;
//        ho.writeMapBegin(null);
//        ho.writeObject(test);
//        ho.writeMapEnd();
//        ho.flush();
//        trace(byteArray.length);
//        byteArray.position = 0;
//        var hi:Hessian2Input = new Hessian2Input(byteArray);
//        hi.readMapStart();
//        var obj:ClothCategory = hi.readObject(ClothCategory) as ClothCategory;
//        hi.readMapEnd();
//        trace(byteArray.toString());
//        trace(byteArray.readUTF());
//        trace(obj);  // проходит
        try{   ///// пробуем прочитать ответ сервера
            var respdata:Object = JSON.parse(event.target.data);
            trace(respdata);
        } catch (e:Error){
            Cc.infoch("server","PARSE ERROR REPLY DATA:"+ e.toString());
        }
    }

    protected function ioErrorHandler(event:IOErrorEvent):void
    {
        // TODO Auto-generated method stub
        trace("ioErrorHandler",event);
        Cc.infoch("server","ioErrorHandler:"+event.toString());

    }

    protected function securityErrorHandler(event:SecurityErrorEvent):void
    {
        // TODO Auto-generated method stub
        trace("securityErrorHandler",event);
        Cc.infoch("server","securityErrorHandler:"+event.toString());

    }

    protected function responseStatusHandler(event:HTTPStatusEvent):void
    {
        // TODO Auto-generated method stub
        trace("responseStatusHandler",event);
//        Cc.infoch("server","responseStatusHandler:"+event.toString());
    }

    protected function statusHandler(event:HTTPStatusEvent):void
    {
//        TODO Auto-generated method stub
        trace("statusHandler",event)
//        Cc.infoch("server","statusHandler:"+event.toString());
    }

    protected function progressHandler(event:ProgressEvent):void
    {
        // TODO Auto-generated method stub
        trace("progressHandler",event)
//        Cc.infoch("server","statusHandler:"+event.toString());
    }

}
}

/**
 * Created with IntelliJ IDEA.
 * User: Gleb
 * Date: 19.04.12
 * Time: 22:07
 * The thought of the sun.
 */
package net.amf {
public class AmfEvent extends Event {
    public var data:Object;
    public static const ERROR:String = "error";
    public static const NET_STATUS:String = "net_status";
    public static const DATA:String = "on_data";
    public static const FAULT:String = "fault";

    public function AmfEvent(type:String, data:Object = null) {
        super(type);
        this.data = data;
    }
    override public function clone():Event {
        return new AmfEvent(this.type, this.data);
    }
}
}

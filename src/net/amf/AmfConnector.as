package net.amf {
//import classes.Call;

public class AmfConnector extends EventDispatcher {
    private var connection:NetConnection;
    private var responder:Responder;
    public var gateway:String;

    public function AmfConnector(gateway:String) {
        this.gateway = gateway;
        responder = new Responder(onResult, onFault);
//			try{
        connection = new NetConnection();
        connection.addEventListener(NetStatusEvent.NET_STATUS, onNetStatus);
        connection.addEventListener(IOErrorEvent.IO_ERROR, onIOError);
        connection.addEventListener(AsyncErrorEvent.ASYNC_ERROR, onAsyncError);
        connection.connect(gateway);

//			}
//			catch(error:Error)
//			{
//                dispatchEvent(new AmfEvent(AmfEvent.ERROR,error));
//				trace(error);
//			}

    }

    private function onAsyncError(event:AsyncErrorEvent):void {
        dispatchEvent(new AmfEvent(AmfEvent.ERROR, event));
        trace("[AMF] :",event);
    }

    private function onIOError(event:IOErrorEvent):void {
        dispatchEvent(new AmfEvent(AmfEvent.ERROR, event));
        trace("[AMF] :",event);
    }

    private function onNetStatus(event:NetStatusEvent):void {
        dispatchEvent(new AmfEvent(AmfEvent.NET_STATUS, event));
        trace("[AMF] :",event);
    }

    private var mainClass:String;
    public function initMainClass(className:String):void{
        mainClass = className;
    }
    public function call(classMethod:String,command:String,data:*=null):void{
		trace("[AMF] :",mainClass,classMethod,data);
        var params:Object = new Object();
        params.command = command;
        params.data = data;
//        Cc.visible = true;
        Cc.add(gateway+ " :: "+mainClass+"."+classMethod+": "+JSON.stringify(params));
        connection.call(mainClass+"."+classMethod, responder, params);
    }


    private function onResult(result:*):void {
        Mouse.cursor = MouseCursor.AUTO;
        Cc.info("Amf result: "+ JSON.stringify(result)) ;
        dispatchEvent(new AmfEvent(AmfEvent.DATA, result));
    }

    private function onFault(fault:Object):void {
        trace("[AMF] onFault: ", String(fault));
        dispatchEvent(new AmfEvent(AmfEvent.FAULT, fault));
        Mouse.cursor = MouseCursor.AUTO
    }
}
}
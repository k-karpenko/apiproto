/**
 * Created with IntelliJ IDEA.
 * User: Gleb
 * Date: 19.04.12
 * Time: 22:07
 * The thought of the sun.
 */
package event {
import flash.events.Event;

public class SocialEvent extends Event {
    public var data:Object;
    public static const ON_USER_PROFILE:String = "on_user_profile";
    public static const ON_APP_INSTALL:String = "on_app_install";
    public static const ON_PHOTO_ALL:String = "on_photo_all";
    public static const GET_PHOTO_ALL:String = "get_photo_all";
    public static const POST_WALL_PHOTO:String = "post_wall_photo";


    public function SocialEvent(type:String, data:Object = null) {
        super(type);
        this.data = data;
    }
    override public function clone():Event {
        return new SocialEvent(this.type, this.data);
    }
}
}

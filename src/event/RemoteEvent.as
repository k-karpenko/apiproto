/**
 * Created with IntelliJ IDEA.
 * User: Gleb
 * Date: 19.04.12
 * Time: 22:07
 * The thought of the sun.
 */
package event {
import flash.events.Event;

public class RemoteEvent extends Event {
    public var data:Object;
    public static const SEND_BODY_PARAM:String = "remote_send_body_params";
    public static const SOCIAL_START:String = "remotre_social_start";
    public static const CUSTOM_START:String = "remotre_custom_start";

    public function RemoteEvent(type:String, data:Object = null) {
        super(type);
        this.data = data;
    }
    override public function clone():Event {
        return new RemoteEvent(this.type, this.data);
    }
}
}

package com.dressformer {

import com.dressformer.connection.IConnection;
import com.dressformer.connection.protocol.APIRequest;
import com.dressformer.connection.protocol.MethodUtils;
import com.dressformer.utils.StringUtils;

import flash.utils.Proxy;
import flash.utils.flash_proxy;

use namespace flash_proxy;

/**
 * Central API Facade
 */
public class API extends Proxy {
    private static var _instance : API;
    private var connection : IConnection;

    function API() {
    }

    public static function getInstance() {
        if ( _instance == null ) {
            _instance = new API();
        }

        return _instance;
    }

    public function getConnection() : IConnection {
        return connection;
    }

    override flash_proxy function callProperty(name:*, ...rest):* {
        var request : APIRequest = new APIRequest( "", MethodUtils.generateCanonicalName( StringUtils.splitByCamelCase(name) ) );
        request.setParameters(rest);
        return this.getConnection().send(request);
    }
}
}

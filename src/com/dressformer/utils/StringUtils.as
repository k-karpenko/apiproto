/**
 * @author nikelin
 * @date 16:39
 */
package com.dressformer.utils {
public class StringUtils {
    private static var camelCaseDelimiters : String = "-_";


    /**
     * Camelize input string
     * @param name Input string
     * @param ucfirst Make first character uppercased
     * @return String
     */
    public static function toCamelCase( name : String, ucfirst : Boolean ) : String {
        var result : String = new String();

        for ( var i : int  = 0; i < name.length; i++ ) {
            var prevChar : String = name.substring(i > 0 ? i - 1 : 0, i > 0 ? i : 1);
            var currChar : String = name.substring(i, i + 1);

            if ( camelCaseDelimiters.indexOf( prevChar ) != -1
                    || ( ucfirst && i == 0 && camelCaseDelimiters.indexOf( currChar ) == -1 ) ) {
                result = result.concat( currChar.toUpperCase() );
            } else if ( camelCaseDelimiters.indexOf( currChar ) == -1 ) {
                result = result.concat(currChar);
            }
        }

        return result;
    }

    public static function fromCamelCase( name : String, delimiter : String ) {
        var result : String = new String();

        var last_delimiter_pos = 0;
        for ( var i : int = 0; i < name.length; i++ ) {
            var currChar : String = name.substring(i, i + 1);

            if ( currChar.toUpperCase() == currChar && i != last_delimiter_pos - 1 ) {
                if ( i > 0 ) {
                    result = result.concat( delimiter );
                    last_delimiter_pos = i;
                }

                result = result.concat( currChar.toLowerCase() );
            } else {
                result = result.concat( currChar.toLowerCase() );
            }
        }

        return result;
    }

    public static function ucfirst( value : String ) : String {
        if ( value.length == 0 ) {
            return value;
        }

        return value.substring(0, 1).toUpperCase().concat( value.substring(1) );
    }

    public static function lcfirst( value : String ) : String {
        if ( value.length == 0 ) {
            return value;
        }

        return value.substring(0, 1).toLowerCase().concat( value.substring(1) );
    }


    public static function splitByCamelCase( origin : String ) : Array {
        return fromCamelCase( origin, "-").split("-");
    }

}
}

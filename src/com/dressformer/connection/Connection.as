/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 6/29/12
 * Time: 7:45 PM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer.connection {
import com.dressformer.connection.protocol.IAPIRequest;
import com.dressformer.connection.protocol.IAPIResponse;
import com.dressformer.connection.protocol.serializing.ISerializer;
import com.dressformer.connection.stream.IDataStreamReader;
import com.dressformer.connection.stream.IDataStreamWriter;

import flash.events.EventDispatcher;

import flash.net.Socket;
import flash.utils.ByteArray;

[Event(type = OPEN)]
[Event(type = CLOSED)]
public class Connection extends EventDispatcher implements IConnection {
    public static var OPEN : String = "Connection.Open";
    public static var CLOSED : String = "Connection.Closed";
    public static var READ_BUFF_SIZE : uint = 8192;

    private var connection : Socket;
    private var serializer : ISerializer;

    public function Connection( serializer : ISerializer ) {
        this.serializer = serializer;
    }

    protected function getSerializer() : ISerializer {
        return this.serializer;
    }

    public function isOpen():Boolean {
        return this.connection == null;
    }

    public function send(request:IAPIRequest):IAPIResponse {
        var data : ByteArray = this.getSerializer().encode(request);
        connection.writeBytes( data, 0, data.length - 1 );
        connection.flush();

        var response : ByteArray = new ByteArray();
        var tempBuff : ByteArray = new ByteArray();
        while ( connection.bytesAvailable > 0 ) {
            connection.readBytes( tempBuff, 0, READ_BUFF_SIZE );
            response.writeBytes(tempBuff, 0, tempBuff.length );
            tempBuff.clear();
        }

        return this.getSerializer().decode(tempBuff);
    }

    public function open(host:String, port:int):void {
        this.connection = new Socket();
        this.connection.connect(host,  port);
    }

    public function close():void {
        connection.close();
    }
}
}

/**
 * @author nikelin
 * @date 16:26
 */
package com.dressformer.connection.stream {
public class StandardDataStreamReader implements IDataStreamReader {
    public function StandardDataStreamReader() {
    }

    public function readString():String {
        return "";
    }

    public function readInt():int {
        return 0;
    }

    public function readBoolean():Boolean {
        return false;
    }

    public function readObject():Object {
        return null;
    }

    public function readArray():Array {
        return null;
    }
}
}

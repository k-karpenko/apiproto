/**
 * @author nikelin
 * @date 16:26
 */
package com.dressformer.connection.stream {
public class StandardDataStreamWriter implements IDataStreamWriter{
    public function StandardDataStreamWriter() {
    }

    public function writeInt(value:int):void {
    }

    public function writeArray(value:Array):void {
    }

    public function writeObject(value:Object):void {
    }

    public function writeString(value:String):void {
    }

    public function writeBoolean(value:Boolean):void {
    }
}
}

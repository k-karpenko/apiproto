/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 6/29/12
 * Time: 7:28 PM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer.connection.stream {
import flashx.textLayout.formats.Float;

public interface IDataStreamReader {

    function readString() : String;

    function readInt() : int;

    function readBoolean() : Boolean;

    function readObject() : Object;

    function readArray() : Array;

}
}

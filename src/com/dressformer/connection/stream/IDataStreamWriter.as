/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 6/29/12
 * Time: 7:28 PM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer.connection.stream {

public interface IDataStreamWriter {

    function writeString( value : String ) : void;

    function writeInt( value : int ) : void;

    function writeArray( value : Array ) : void;

    function writeObject( value : Object ) : void;

    function writeBoolean( value : Boolean ) : void;

}
}

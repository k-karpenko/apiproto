/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 6/29/12
 * Time: 7:23 PM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer.connection.protocol {
public interface IAPIResponse {

    function asError() : IAPIResponseError;

    function isError() : Boolean;

    function getParametersCount() : int;

    function getParameter( idx : int ) : *;

    function getParameters() : Array;

}
}

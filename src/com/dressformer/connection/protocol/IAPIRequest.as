/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 6/29/12
 * Time: 7:23 PM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer.connection.protocol {
public interface IAPIRequest {

    function getNamespace() : String;

    function getMethod() : String;

    function setParameters( value : Array ) : void;

    function getParameters() : Array;

}
}

/**
 * @author nikelin
 * @date 17:05
 */
package com.dressformer.connection.protocol {
    public class APIRequest implements IAPIRequest{
        private var namespace : String;
        private var method : String;
        private var parameters : Array;

        public function APIRequest( namespace : String,  method : String, parameters : Array = null ) {
            this.parameters = parameters || new Array();
            this.namespace = namespace;
            this.method = method;
        }

        public function getNamespace():String {
            return this.namespace;
        }

        public function getMethod():String {
            return this.method;
        }

        public function setParameters( value : Array ) : void {
            this.parameters = parameters;
        }

        public function getParameters() : Array {
            return this.parameters;
        }
    }
}

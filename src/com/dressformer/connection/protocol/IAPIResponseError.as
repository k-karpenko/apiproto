/**
 * @author nikelin
 * @date 17:08
 */
package com.dressformer.connection.protocol {
    public interface IAPIResponseError extends IAPIResponse {

        function getMessage() : String;

        function getCode() : int;

    }
}

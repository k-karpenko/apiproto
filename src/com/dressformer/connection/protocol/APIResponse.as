/**
 * @author nikelin
 * @date 17:07
 */
package com.dressformer.connection.protocol {
    public class APIResponse implements IAPIResponse{
        private var parameters : Array;

        public function APIResponse() {
            this.parameters = new Array();
        }

        public function isError():Boolean {
            return false;
        }

        public function getParameters():Array {
            return this.parameters;
        }

        public function getParametersCount():int {
            return this.parameters.length;
        }

        public function getParameter(idx:int):* {
            return this.parameters[idx];
        }

        public function asError():IAPIResponseError {
            return this as IAPIResponseError;
        }
    }
}

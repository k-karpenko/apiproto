/**
 * @author nikelin
 * @date 17:24
 */
package com.dressformer.connection.protocol.serializing {
import avmplus.metadataXml;

import mx.core.mx_internal;
    import flash.utils.ByteArray;

    public class StandardSerializer implements ISerializer {

        public function encode(object:*):ByteArray {
            var byteArray : ByteArray = new ByteArray();
            /***
             * ByteArray filling based on reflection metadata
             * which can be accessed through metadataXml, for example.
             *
             * If we encode complex datatype, as user defined class, for example,
             * we need to:
             * 1. Save it's class name
             * 2. Make sure that all classes which could be given to serializer
             * presents in a flash runtime (already loaded by classloader)
             * 3. Filter not serializable entities (resources, connections, etc.)
             */
        }

        public function decode(data:ByteArray):* {

        }

    }
}

/**
 * @author nikelin
 * @date 17:17
 */
package com.dressformer.connection.protocol.serializing {
    import flash.utils.ByteArray;

    public interface ISerializer {

        function encode( object : * ) : ByteArray;

        function decode( data : ByteArray ) : *;

    }
}

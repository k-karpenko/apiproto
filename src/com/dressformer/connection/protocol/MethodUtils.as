/**
 * @author nikelin
 * @date 16:48
 */
package com.dressformer.connection.protocol {
import com.dressformer.utils.StringUtils;

public final class MethodUtils {

        public static function generateCanonicalName( path : Array ) : String {
            return path.slice(1).map(function(arg : String, idx : int,  array : *) {
                    return StringUtils.ucfirst(arg);
                }).join(".").concat(".").concat( StringUtils.ucfirst(path[0]) );
        }

    }
}

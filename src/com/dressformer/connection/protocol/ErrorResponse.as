/**
 * @author nikelin
 * @date 17:07
 */
package com.dressformer.connection.protocol {
    public class ErrorResponse extends APIResponse implements IAPIResponseError{
        private var message : String;
        private var code : int;

        override public function isError():Boolean {
            return true;
        }

        public function getMessage():String {
            return this.message;;
        }

        public function getCode():int {
            return this.code;
        }

    }
}

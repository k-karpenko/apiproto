/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 6/29/12
 * Time: 7:23 PM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer.connection {
import com.dressformer.connection.protocol.IAPIRequest;
import com.dressformer.connection.protocol.IAPIResponse;
import com.dressformer.connection.stream.IDataStreamReader;
import com.dressformer.connection.stream.IDataStreamWriter;

import flash.events.IEventDispatcher;

public interface IConnection extends IEventDispatcher {

    function isOpen() : Boolean;

    function send( request : IAPIRequest ) : IAPIResponse;

    function open( host : String,  port : int ) : void;

    function close() : void;

}
}

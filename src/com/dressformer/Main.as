/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 6/29/12
 * Time: 5:51 PM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer {
import com.dressformer.connection.Connection;
import com.dressformer.connection.IConnection;
import com.dressformer.connection.protocol.serializing.StandardSerializer;
import com.dressformer.connection.stream.StandardDataStreamReader;
import com.dressformer.connection.stream.StandardDataStreamWriter;

import flash.display.Sprite;

public final class Main extends Sprite {
        public function Main() {
            main();
        }

        public static function main() : void {
            var connection : IConnection = new Connection( new StandardSerializer() );

            var result = API.getInstance().saveProfileField(1);
        }
    }

}

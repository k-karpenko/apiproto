/**
 * Created with IntelliJ IDEA.
 * User: cyril
 * Date: 6/29/12
 * Time: 7:27 PM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer.objects {
import com.dressformer.connection.stream.IDataStreamReader;
import com.dressformer.connection.stream.IDataStreamWriter;

public interface IDTO {

    function serialize( stream : IDataStreamReader ) : void;

    function deserialize( stream : IDataStreamWriter ) : void;

}
}

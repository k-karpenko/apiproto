/**
 * Created with IntelliJ IDEA.
 * User: soul
 * Date: 7/10/12
 * Time: 8:51 AM
 * To change this template use File | Settings | File Templates.
 */
package com.dressformer {
import flash.events.Event;
import flash.events.HTTPStatusEvent;
import flash.events.IOErrorEvent;
import flash.events.ProgressEvent;
import flash.events.SecurityErrorEvent;
import flash.net.URLLoader;
import flash.net.URLRequest;
import flash.net.URLRequestHeader;
import flash.net.URLRequestMethod;
import flash.net.URLVariables;
import flash.utils.ByteArray;
import flash.utils.IDataOutput;

public class HessianExample {
    public function HessianExample() {
    }
    import hessian.io.Hessian2Output;
    import hessian.io.HessianOutput;

    import mx.events.FlexEvent;

    protected function application1_creationCompleteHandler(event:FlexEvent):void
    {




        var urlVars:URLVariables = new URLVariables();
        urlVars.app = "google";
        urlVars.returnTo = "punk.com";
        // Create a URL request
        var urlRequest:URLRequest = new URLRequest("http://80.82.81.238:8181/api/g/Auth.OAuth.Request.do?"+urlVars.toString());
        urlRequest.method = URLRequestMethod.POST;
        urlRequest.requestHeaders.push(new URLRequestHeader('X-Requested-With', 'XMLHttpRequest-Hessian-Format'));
        trace(urlRequest.url);
        // Prepare hessian data
        var byteArray:ByteArray = new ByteArray();
        var io:IDataOutput = byteArray;
        var test:Object = new Object();
        test.app = "google";
        test.returnTo = "sex.com";
        var ho:HessianOutput = new HessianOutput(io);

        ho.startReply();
        ho.writeString("test");
        ho.writeObject(test);
        ho.writeObjectEnd();
        ho.completeReply();
//				var sendStr:Object = byteArray.readUTF();
        var sendStr:Object = byteArray.toString();
        trace("send data string: ",sendStr," byteArray length:",byteArray.length);
        urlRequest.data = sendStr;

        var urlLoader:URLLoader = new URLLoader();
//				urlLoader.dataFormat = URLLoaderDataFormat.BINARY;
        urlLoader.addEventListener(Event.COMPLETE, chunkComplete);
        urlLoader.addEventListener(IOErrorEvent.IO_ERROR, ioErrorHandler);
        urlLoader.addEventListener(SecurityErrorEvent.SECURITY_ERROR, securityErrorHandler);
        //urlLoader.addEventListener(HTTPStatusEvent.HTTP_RESPONSE_STATUS, responseStatusHandler);
        urlLoader.addEventListener(HTTPStatusEvent.HTTP_STATUS, statusHandler);
        urlLoader.addEventListener(ProgressEvent.PROGRESS, progressHandler);
        urlLoader.load(urlRequest)



    }

    protected function chunkComplete(event:Event):void
    {
        // TODO Auto-generated method stub
        trace("complete",event)
        trace("reply data: ",event.target.data)

    }

    protected function ioErrorHandler(event:IOErrorEvent):void
    {
        // TODO Auto-generated method stub
        trace("ioErrorHandler",event)

    }

    protected function securityErrorHandler(event:SecurityErrorEvent):void
    {
        // TODO Auto-generated method stub
        trace("securityErrorHandler",event)

    }

    protected function responseStatusHandler(event:HTTPStatusEvent):void
    {
        // TODO Auto-generated method stub
        trace("responseStatusHandler",event)
    }

    protected function statusHandler(event:HTTPStatusEvent):void
    {
        // TODO Auto-generated method stub
        trace("statusHandler",event)
    }

    protected function progressHandler(event:ProgressEvent):void
    {
        // TODO Auto-generated method stub
        trace("progressHandler",event)
    }


}
}

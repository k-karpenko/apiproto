/**
 * Created by IntelliJ IDEA.
 * User: soul
 * Date: 6/20/12
 * Time: 10:15 AM
 */
package {
import com.junkbyte.console.Cc;

import event.RemoteEvent;

import flash.events.EventDispatcher;

import mx.core.FlexGlobals;

import vk.VkService;

public class SocialService extends EventDispatcher{

    public var service:VkService;
    public var connect:Boolean;
    public function SocialService(){
        im = this;
        service = new VkService();
        var param:Object =  FlexGlobals.topLevelApplication.parameters;
        if (param.api_url){
            connect = true;
            service.params.initFlashVars(param);
            var first_req:Object = JSON.parse(param.api_result);
            Cc.info("Here is a full api request: ", param);
            this.dispatchEvent(new RemoteEvent(RemoteEvent.SOCIAL_START));
        } else {
            connect = false;
//            Cc.log("runMode: local" + ExternalInterface.call('window.location.href.toString'));
            var localRun:Object = new Object();
            localRun.viewer_id = "6270810";
            localRun.user_id = "6270810";
            localRun.api_id = "3004508";
            localRun.sid = "35fcc89516933d9ab5320326371f390dbb962bb7000dd9db96586ff1613fc3";
            localRun.secret = "db17fcfad4";
//            localRun.access_token = "d0896d0b80f55ef1d015ac647bd0fb1a0ddd0d6d0d6e6555edb2410f8134128";
            service.params.initFlashVars(localRun);
            service.requst.getUserProfile();
            this.dispatchEvent(new RemoteEvent(RemoteEvent.CUSTOM_START,{mode:"debug"}));
        }


    }
    public static var im:SocialService;

    public static function get access_token():* {
        return im.service.params.access_token;
    }
}
}

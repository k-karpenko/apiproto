/**
 * Created by IntelliJ IDEA.
 * User: soul
 * Date: 6/22/12
 * Time: 11:34 AM
 */
package vk {

public class VkParams {
    /*это адрес сервиса API, по которому необходимо осуществлять запросы.*/
    public var api_url:String;
    /*это id запущенного приложения.*/
    public var api_id:String;
    /* это id пользователя, со страницы которого было запущено приложение. Если приложение запущено не со страницы пользователя, то значение равно 0*/
    public var user_id:String;
    /* id сессии для осуществления запросов к AP*/
    public var sid:String;
    /*Секрет, необходимый для осуществления подписи запросов к API*/
    public var secret:String;
    /*– это id группы, со страницы которой было запущено приложение. Если приложение запущено не со страницы группы, то значение равно 0.*/
    public var group_id:String ;
    /*– это id пользователя, который просматривает приложение.*/
    public var viewer_id:String;
    /*  – если пользователь установил приложение – 1, иначе – 0.*/
    public var is_app_user:String;
    /* – это тип пользователя, который просматривает приложение (возможные значения описаны ниже).*/
    public var viewer_type:String;
    /*это ключ, необходимый для авторизации пользователя на стороннем сервере (см. описание ниже).*/
    public var auth_key:String;
    /*это id языка пользователя, просматривающего приложение (см. список языков ниже).*/
    public var language:String;
    /*это результат первого API-запроса, который выполняется при загрузке приложения (см. описание ниже).*/
    public var api_result:String;
    /*битовая маска настроек текущего пользователя в данном приложении (подробнее см. в описании метода getUserSettings).*/
    public var api_settings:String;
    /*это обозначение места откуда пользователь перешёл в приложение (см. список значений ниже).*/
    public var referrer:String;
    /*ключ доступа для использования упрощённого вызова методов API.*/
    public var access_token:String;

    public function VkParams() {

    }

    public function initFlashVars(param:Object):void{
        api_url = param.api_url;
        api_id = param.api_id;
        user_id = param.user_id;
        group_id = param.group_id;
        viewer_id = param.viewer_id;
        is_app_user = param.is_app_user;
        viewer_type = param.viewer_type;
        auth_key = param.auth_key;
        language = param.language;
        api_result = param.api_result;
        api_settings = param.api_settings;
        referrer = param.referrer;
        access_token = param.access_token;
        sid = param.sid;
        secret = param.secret;
        if (api_result) 
           AppFacade.data.user.update(JSON.parse(api_result).response[0]);
        if (api_url)
            VkontakteApplicationOptions.API_URL = api_url;
        VkontakteApplicationOptions.APPLICATION_ID =  api_id;
        VkontakteApplicationOptions.APPLICATION_SECRET = secret;
        VkontakteApplicationOptions.SESSION_ID = sid;
        VkontakteApplicationOptions.APPLICATION_SECURE_KEY = access_token;
        VkontakteApplicationOptions.VIEWER_ID = viewer_id;
        VkontakteApplicationOptions.RESPONSE_FORMAT = "json";

        var params:Object =
        {
            api_url: api_url,
            api_id: api_id,
            viewer_id: viewer_id,
            secret: secret,
            sid: sid
        };
//         Vkontakte.getInstance().applicationParameters = new ApplicationParameters(params);



    }

}
}

/**
 * Created by IntelliJ IDEA.
 * User: soul
 * Date: 6/22/12
 * Time: 12:12 PM
 */
package vk {


public class VKWallInvitation {
    private var jpgImage:ByteArray;
    private var name:String;
    public function VKWallInvitation(data:Object):void {
        jpgImage = JPEGEncoder.encode(data.bitmapData,10);
        name = data.name;
        var requestLoader : RequestLoader = new RequestLoader(PhotoMethod.GET_WALL_UPLOAD_SERVER);
        requestLoader.responder.addEventListener(Event.COMPLETE, onGetWallUploadServer);
        requestLoader.responder.addEventListener(ErrorEvent.ERROR, onErrorHandler);
        requestLoader.load();
        Cc.add("start post photo") 
    }
    protected function onErrorHandler(event:ErrorEvent):void {
        Cc.info("VkWallPost onErrorHandler",event)
    }
    protected function onGetWallUploadServer (event:Event):void {
        var responder : RequestResponder = event.target as RequestResponder;
        responder.removeEventListener(Event.COMPLETE, onGetWallUploadServer);
        responder.removeEventListener(ErrorEvent.ERROR, onErrorHandler);
        responder.request.close();
        var data:Object = JSON.parse(responder.data);
        var url:String = data.response.upload_url;

        var loader:URLLoader = new URLLoader();
        loader.addEventListener(Event.COMPLETE, jpegUploadCompleteHandler);
        loader.addEventListener(HTTPStatusEvent.HTTP_STATUS, jpegUploadHTTPStatusHandler);
        loader.addEventListener(ProgressEvent.PROGRESS, jpegUploadProgressHandler);

        var mdata:MultipartData = new MultipartData();

//        var urlRequest:URLRequest = new URLRequest(url);
//        urlRequest.method = URLRequestMethod.POST;
//        urlRequest.requestHeaders.push(new URLRequestHeader("Content-type", "multipart/form-data; boundary=" + MultipartData.BOUNDARY));
//
//        mdata.addFile(jpgImage,);
//
//        urlRequest.data = mdata;
//        loader.load(urlRequest);



        var fileContentType:String = 'image/jpeg';
        var mpLoader:MultipartURLLoader = new MultipartURLLoader();
        mpLoader.addEventListener(Event.COMPLETE, jpegUploadHTTPStatusHandler);
        mpLoader.addFile(jpgImage, "image.jpg", "file1", fileContentType);
        mpLoader.load(url);
        Cc.add("Upload server ok, start upload")

    }


    protected  function getWallUploadServerErrorHandler ( data : Object ) : void {
        Cc.add('getWallUploadServerErrorHandler ');
    }

    protected  function jpegUploadHTTPStatusHandler (e : Event) : void {
        Cc.add('jpegUploadHTTPStatusHandler');
    }

    protected static function jpegUploadProgressHandler (e : Event) : void {
        Cc.add('jpegUploadProgressHandler');
    }

    protected  function saveWallPhotoSuccessHandler ( data : Object ) : void {
        Cc.add('saveWallPhotoSuccessHandler');
    }

    protected  function saveWallPhotoErrorHandler ( data : Object ) : void {
        Cc.add('saveWallPhotoErrorHandler');
    }

//    protected  function jpegUploadCompleteHandler (e : Event) : void {
    private function jpegUploadCompleteHandler(e:Event):void {
        //Cc.add(e.currentTarget.loader.data)
        var data:Object = JSON.parse(e.currentTarget.loader.data );
//        Cc.info("serverResponseDecoded: ", data.server, data.photos_list, data.hash);
//        var data:Object = JSON.parse(e.target.data as String);
        //var photo:Object = JSON.parse(data.photo);
        var variables : URLVariables = new URLVariables();
        variables["wall_id"] = VkontakteApplicationOptions.VIEWER_ID;
        variables["server"] = data.server;
        variables["photo"] = data.photo;
        variables["hash"] = "";//data.hash;
        variables["message"] = "Уррра заработало";
        var requestLoader : RequestLoader = new RequestLoader(WallMethod.SAVE_POST);
        requestLoader.responder.addEventListener(Event.COMPLETE, onWallSavePost);
        requestLoader.responder.addEventListener(ErrorEvent.ERROR, onErrorHandler);
        requestLoader.load(variables);
        //Cc.add('jpegUploadCompleteHandler: '+variables);
        //vkApiConnection.api('wall.savePost', {'wall_id':flashVars.viewer_id, 'server':data.server, 'photo':data.photo, 'hash':'', 'message':'Уррра заработало'}, saveWallPhotoSuccessHandler, saveWallPhotoErrorHandler);
    }

    private function onWallSavePost(event:Event):void {
        var responder : RequestResponder = event.target as RequestResponder;
        responder.removeEventListener(Event.COMPLETE, onWallSavePost);
        responder.removeEventListener(ErrorEvent.ERROR, onErrorHandler);
        responder.request.close();
        var data:Object = JSON.parse(responder.data);
//        dispatchEvent(new SocialEvent(SocialEvent.ON_PHOTO_ALL,data.response));
        Cc.info("onWallSavePost: ",responder.data);
    }


}
}

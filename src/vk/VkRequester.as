/**
 * Created with IntelliJ IDEA.
 * User: Gleb
 * Date: 20.04.12
 * Time: 19:26
 * The thought of the sun.
 */
package vk {
public class VkRequester {

    public function VkRequester() {
    }


    private var eventDispatcher:EventDispatcher;
    public function initDispacher(eventDispatcher:EventDispatcher):void{
        this.eventDispatcher = eventDispatcher;
    }
    private function onErrorHandler(event:ErrorEvent):void {
        trace(event);
    }
    public function getAppInstall(e:*=null):void{
        var requestLoader : RequestLoader = new RequestLoader(UserMethod.IS_APP_USER);
        requestLoader.responder.addEventListener(Event.COMPLETE,onAppInstall);
        requestLoader.responder.addEventListener(ErrorEvent.ERROR, onErrorHandler);
        requestLoader.load();
    }

    private function onAppInstall(event : Event):void {
        var responder : RequestResponder = event.target as RequestResponder;
        responder.removeEventListener(Event.COMPLETE, onUserProfileHandler);
        responder.removeEventListener(ErrorEvent.ERROR, onErrorHandler);
        responder.request.close();
        var data:Object = JSON.parse(responder.data);
        eventDispatcher.dispatchEvent(new SocialEvent(SocialEvent.ON_APP_INSTALL,new Boolean(data.response)));
    }

    public function getUserProfile(e:*=null):void{
        var requestLoader : RequestLoader = new RequestLoader(UserMethod.GET_PROFILES);
        var variables : URLVariables = new URLVariables();
        variables["uids"] = VkontakteApplicationOptions.VIEWER_ID;
        variables["fields"] = " first_name, last_name, nickname, screen_name, sex, city, photo, photo_medium, photo_big, rate, contacts, counters.";
        requestLoader.responder.addEventListener(Event.COMPLETE, onUserProfileHandler);
        requestLoader.responder.addEventListener(ErrorEvent.ERROR, onErrorHandler);
        requestLoader.load(variables);
    }
    private function onUserProfileHandler(event : Event) : void {


            var responder : RequestResponder = event.target as RequestResponder;
            responder.removeEventListener(Event.COMPLETE, onUserProfileHandler);
            responder.removeEventListener(ErrorEvent.ERROR, onErrorHandler);
            responder.request.close();
            var data:Object = JSON.parse(responder.data);
        if (data.error){
            Cc.add(data.error.error_msg)
        }else {
            AppFacade.data.user.update(data.response[0]);
            eventDispatcher.dispatchEvent(new SocialEvent(SocialEvent.ON_USER_PROFILE,data.response[0]))
        }
    }

    public function photoGetAll(e:*=null):void {
        var requestLoader : RequestLoader = new RequestLoader(PhotoMethod.GET_ALL);
        var variables : URLVariables = new URLVariables();
        variables["owner_id"] = VkontakteApplicationOptions.VIEWER_ID;
        variables["extended"] = "1";
        variables["count"] = "24";
        requestLoader.responder.addEventListener(Event.COMPLETE, onGetPhotoAll);
        requestLoader.responder.addEventListener(ErrorEvent.ERROR, onErrorHandler);
        requestLoader.load(variables);
    }

    private function onGetPhotoAll(event:Event):void {
        var responder : RequestResponder = event.target as RequestResponder;
        responder.removeEventListener(Event.COMPLETE, onGetPhotoAll);
        responder.removeEventListener(ErrorEvent.ERROR, onErrorHandler);
        responder.request.close();
        var data:Object = JSON.parse(responder.data);
        eventDispatcher.dispatchEvent(new SocialEvent(SocialEvent.ON_PHOTO_ALL,data.response));
    }

    public function postPhoto(e:*=null):void {
        var requestLoader : RequestLoader = new RequestLoader(PhotoMethod.GET_WALL_UPLOAD_SERVER);
        var variables : URLVariables = new URLVariables();
        variables["owner_id"] = VkontakteApplicationOptions.VIEWER_ID;
        variables["extended"] = "1";
        variables["count"] = "24";
        requestLoader.responder.addEventListener(Event.COMPLETE, onGetPhotoAll);
        requestLoader.responder.addEventListener(ErrorEvent.ERROR, onErrorHandler);
        requestLoader.load(variables);
    }
    private function postPostPhoto(event:Event):void {
        var responder : RequestResponder = event.target as RequestResponder;
        responder.removeEventListener(Event.COMPLETE, onGetPhotoAll);
        responder.removeEventListener(ErrorEvent.ERROR, onErrorHandler);
        responder.request.close();
        var data:Object = JSON.parse(responder.data);
        eventDispatcher.dispatchEvent(new SocialEvent(SocialEvent.ON_PHOTO_ALL,data.response));
    }





}
}
